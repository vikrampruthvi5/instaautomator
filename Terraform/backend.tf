terraform {
  backend "s3" {
    bucket = "automator-terraform"
    key    = "tfstate/automator.tfstate"
    region = "us-east-1"
  }
}